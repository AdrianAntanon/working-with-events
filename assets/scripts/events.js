const button = document.querySelector("button");

// button.onclick = () => {}

const buttonClickHandler = (ev) => {
  console.log(ev);
};

// button.onclick = buttonClickHandler;

// const boundFn = buttonClickHandler.bind(this);

button.addEventListener("click", buttonClickHandler);

setTimeout(() => {
  button.removeEventListener("click", buttonClickHandler);
}, 2000);

const form = document.querySelector("form");

form.addEventListener("submit", (ev) => {
  ev.preventDefault();
  console.log(ev);
});

const div = document.querySelector("div");

div.addEventListener(
  "click",
  (ev) => {
    console.log(ev);
  },
  true
);
// El tercer argumento que le pasamos viene marcado por default en false, y se utiliza para ordenar el orden de aparición de los eventos

const listItems = document.querySelectorAll("li");

listItems.forEach((listItem) => {
  listItem.addEventListener("click", (ev) => {
    ev.target.classList.toggle("highlight");
  });
});
